========================
 GtkSourceView Darkblue
========================

Simple port of `darkblue.vim <http://ftp.stust.edu.tw/vim/runtime/colors/darkblue.vim>`_
for `GtkSourceView <https://wiki.gnome.org/Projects/GtkSourceView/>`_.

References
==========

* `GtkSourceView language specification files <https://git.gnome.org/browse/gtksourceview/tree/data/language-specs>`_
* `GtkSourceView style reference <https://developer.gnome.org/gtksourceview/3.14/style-reference.html>`_
